// No.1 Array To Object
console.log('No.1')
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear() // (tahun sekarang)

    var length = arr.length
    for (x = 0; x < length; x++) {
        var age = thisYear - arr[x][3];

        age = arr[x][3] == undefined || age <= 0 ? 'Invalid Birth Year' : age;
        var dataObj = {
            firstName: arr[x][0],
            lastName: arr[x][1],
            gender: arr[x][2],
            age: age
        }
        console.log(dataObj);
    }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)

arrayToObject([])

// No.2 Shopping Time
console.log('No.2')
function shoppingTime(memberId, money) {
    var saldo = money;
    var cart = [];
    var product = [
        ["Sepatu Stacattu", 1500000],
        ["Baju Zoro", 500000],
        ["Baju H&N", 250000],
        ["Sweater Uniklooh", 175000],
        ["Casing Handphone", 50000]
    ];
    var length = product.length;

    for (x = 0; x < length; x++) {
        if (saldo >= product[x][1]) {
            cart.push(product[x][0])
            saldo = saldo - product[x][1]
        }
    }

    var result = {
        memberId: memberId,
        money: money,
        listPurchased: cart,
        changeMoney: saldo
    }

    if (memberId == undefined || memberId == "") {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else if (cart.length <= 0) {
        return 'Mohon maaf, uang tidak cukup';
    } else {
        return result
    }

}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

// No.3 Naik Angkot
console.log('No.3')
function naikAngkot(arrPenumpang) {
    var jmlPenumpang = arrPenumpang.length;
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var jmlRute = rute.length;
    var tarif = 2000;
    var result = [];

    for (x = 0; x < jmlPenumpang; x++) {
        var naikDari = rute.indexOf(arrPenumpang[x][1]);
        var tujuan = rute.indexOf(arrPenumpang[x][2]);
        var rutePenumpang = [];
        for (y = 0; y < jmlRute; y++) {
            if(y >= naikDari && y <= tujuan){
                rutePenumpang.push(rute[y])
            }
        }
        var biaya = (rutePenumpang.length - 1) * tarif;

        var penumpangObj = {
            penumpang : arrPenumpang[x][0],
            naikDari : arrPenumpang[x][1],
            tujuan : arrPenumpang[x][2],
            bayar : biaya
        }

        result.push(penumpangObj)
    }
    
    return result
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));
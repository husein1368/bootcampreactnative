// No.1 Arrow Function
console.log('No.1')
const golden = function goldenFunction() {
  console.log("this is golden!!")
} // ES5

const silver = () => {
  console.log("this is silver functions !!");
} // ES6

//golden();
silver();

// No.2 Object Literal Es6
console.log('No.2')
const newFunction = function literals(firstName, lastName) {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function () {
      console.log(firstName + " " + lastName)
      return
    }
  }
} // ES5
//newFunction("William", "Imoh").fullName()

const newFunctions = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName() {
      console.log(firstName + " " + lastName)
      return
    }
  }
} // ES6
newFunctions("william", "Imoh").fullName();

// No.3 Destructing
console.log('No.3')
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation);

// No.4 Array Spreading
console.log('No.4')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east) // ES5
const combined = [...west, ...east] // ES6
console.log(combined)

// No.5 Template Literals
console.log('No.5')
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +
  'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
  'incididunt ut labore et dolore magna aliqua. Ut enim' +
  ' ad minim veniam'; // ES5
const after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`; // ES6
console.log(after)

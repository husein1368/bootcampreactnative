// No.1 Looping While
console.log('No.1');
console.log('LOOPING PERTAMA');
var flag = 1;
while (flag <= 20) {
    if (flag % 2 == 0) {
        console.log(flag + ' - I Love Coding');
    }
    flag++
}
console.log('LOOPING KEDUA');
var flag2 = 20;
while (flag2 > 0) {
    if (flag2 % 2 == 0) {
        console.log(flag2 + ' - I Will Become a mobile developer');
    }
    flag2--
}

// No.2 Looping For
console.log('No.2');
for (i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 != 0) {
        console.log(i + ' - I Love Coding')
    } else if (i % 2 == 0) {
        console.log(i + ' - Berkualitas')
    } else if (i % 2 != 0) {
        console.log(i + ' - Santai')
    } else {
        console.log('other' + i)
    }
}
// No.3 Persegi Panjang
console.log('No.3')
var x = 1;
var p = 8;
var l = 4;
var line = '';
while (x <= l) {
    var y = 1;
    while (y <= p) {
        line += '#';
        y++;
    }
    console.log(line)
    line = '';
    x++;
}
// No.4 Tangga
console.log('No.4')
var q = 1;
var size = 7;
var line = '';
while (q <= size) {
    line += '#'
    console.log(line)
    q++;
}
// No.5 Papan Catur
console.log('No.5')
var i = 1;
var size = 8;
var line = '';
while (i <= size) {
    var j = 1;
    while (j <= size) {
        if (i % 2 == 0) {
            if (j % 2 == 0) {
                line += ' ';
            }
            else {
                line += '#'
            }
        }
        else {
            if (j % 2 == 0) {
                line += '#'
            } else {
                line += ' '
            }
        }
        j++;
    }
    console.log(line)
    line = '';
    i++;
}
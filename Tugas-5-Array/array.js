// No.1
console.log('No.1 Range')
function range(startNum, finishNum) {
    var numbers = [];
    var x = startNum;
    var y = finishNum;
    
    if(x == undefined || y == undefined){
        numbers.push(-1);
    } else if (startNum < finishNum) {
        for (x; x <= y; x++) {
            numbers.push(x);
        }
    } else {
        for (x; x >= y; x--) {
            numbers.push(x);
        }
    }

    return numbers;
}

console.log(range(1, 10)); // Ascending
console.log(range(1)); // -1
console.log(range(11, 18)); // Ascending
console.log(range(54, 50)); // Descending
console.log(range()); // -1

// No.2
console.log('No.2 Range With Step')
function rangeWithStep(startNum, finishNum, step) {
    var numbers = [];
    var x = startNum;
    var y = finishNum;
    var z = step;
    
    if (startNum < finishNum) {
        for (x; x <= y; x = x + z) {
            numbers.push(x);
        }
    } else {
        for (x; x >= y; x = x - z) {
            numbers.push(x);
        }
    }

    return numbers;
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4)) 

// No.3
console.log('No.3 Sum of Range')
function sum(startNum, finishNum, step = 1){
    var numbers = [];
    var x = startNum;
    var y = finishNum;
    var z = step;

    if(x == undefined && y == undefined){
        return numbers.reduce((a, b) => a + b, 0);    
    }else if(x == undefined || y == undefined){
        numbers.push(step);
    }else if (startNum < finishNum) {
        for (x; x <= y; x = x + z) {
            numbers.push(x);
        }
    } else {
        for (x; x >= y; x = x - z) {
            numbers.push(x);
        }
    }

    return numbers.reduce((a, b) => a + b, 0);
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// No.4
console.log('No.4 Array Multidimensi')
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(input){
    var length = input.length
    for (x=0; x< length;x++){
        console.log('Nomor ID: '+input[x][0]);
        console.log('Nama Lengkap: '+input[x][1])
        console.log('TTL: '+input[x][2]+', '+input[x][3])
        console.log('Hobi: '+input[x][4])
        console.log('')
    }

}

dataHandling(input);

// No. 5
console.log('No.5 Balik Kata')
function balikKata(input){
    var length = input.length
    var word = "";
    for (x=length-1;x>=0;x--){
        word += input[x];
    }
    return word;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// No.6
console.log('No.6 metode Array')
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input){
    var data = input;
    var nama = input[1] + 'Elsharawy';
    var prov = 'Provinsi ' + input[2];
    var jk = 'Pria';
    var sch = 'SMA Internasional Metro';

    data.splice(1,1, nama);
    data.splice(2,1, prov);
    data.splice(4,1, jk, sch);

    console.log(data); // array baru

    var date = input[3];
    var newDate = date.split("/");
    var month = '';

    if (newDate[1] >= 1 && newDate[1] <= 12) {
        switch (newDate[1]) {
            case '01': {
                month = 'Januari';
                break;
            }
            case '02': {
                month = 'Februari';
                break;
            }
            case '03': {
                month = 'Maret';
                break;
            }
            case '04': {
                month = 'April';
                break;
            }
            case '05': {
                month = 'Mei';
                break;
            }
            case '06': {
                month = 'Juni';
                break;
            }
            case '07': {
                month = 'Juli';
                break;
            }
            case '08': {
                month = 'Agustus';
                break;
            }
            case '09': {
                month = 'September';
                break;
            }
            case '10': {
                month = 'Oktober';
                break;
            }
            case '11': {
                month = 'November';
                break;
            }
            case '12': {
                month = 'Desember';
                break;
            }
        }
    }
    console.log(month);

    var joinDate = newDate.join('-');
    console.log(joinDate);

    var sortDate = newDate.sort(function (value1, value2) { return value2 - value1 } ) 
    console.log(sortDate);

    var sliceName = nama.slice(0, 15);
    console.log(sliceName);

}
dataHandling2(input);
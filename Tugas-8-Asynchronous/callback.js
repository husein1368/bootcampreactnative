function readBooks(time, book, callback){
    console.log(`Saya Membaca ${book.name}`);
    setTimeout(function(){
        let sisaWaktu = 0;
        if(time > book.timeSpent){
            sisaWaktu = time - book.timeSpent;
            console.log(`Saya Sudah Membaca ${book.name}, Sisa Waktu Saya ${sisaWaktu}`);
            callback(sisaWaktu);
        }else{
            console.log('Waktu Saya Habis');
            callback(time);
        }
    }, book.timeSpent)
}

module.exports = readBooks
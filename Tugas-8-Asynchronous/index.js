// No.1 Callback Baca Buku
let readBooks = require('./callback.js');
let books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
];
let length = books.length;
let waktu = 10000; // waktu untuk membaca buku
for (i = 0; i < length; i++) {
    let buku = books[i];
    readBooks(waktu, buku, function (check) {
        console.log(check);
    })
}
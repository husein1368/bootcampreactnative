// No.2 Promise Baca Buku
let readBooksPromise = require('./promise.js');
let books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
];
let length = books.length;
let waktu = 10000; // waktu untuk membaca buku
for (i = 0; i < length; i++) {
    let buku = books[i];
    readBooksPromise(waktu, buku)
    .then(function (fulfilled) {
        console.log(fulfilled);
    })
    .catch(function (error) {
        console.log(error.message);
    });
}
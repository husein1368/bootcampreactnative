// No.1 Animal Class
// Release 0
class Animal {
    constructor(animalName) {
        this.name = animalName;
        this.legs = 4;
        this.cold_blooded = false;
    }
}

let sheep = new Animal("Shaun");
console.log(sheep.name); // Ambil data dari class constructor
console.log(sheep.legs); // Ambil data dari class constructor
console.log(sheep.cold_blooded); // Ambil data dari class constructor

// Release 1
class Ape extends Animal { // class Ape Inheritance class Animal
    constructor(animalName) {
        super(animalName);
        this.legs = 2; // Replace sifat turunan dari parent class
    }
    yell() {
        let yelling = 'Auooo';
        return yelling;
    }
}
let sungokong = new Ape("kera sakti")
console.log(sungokong.name);
console.log(sungokong.legs);
console.log(sungokong.cold_blooded);
console.log(sungokong.yell()); // Auooo 

class Frog extends Animal { // class Frog Inheritance class Animal
    constructor(animalName) {
        super(animalName);
    }
    jump() {
        let jumping = 'Hop Hop';
        return jumping;
    }
}
let kodok = new Frog("buduk");
console.log(kodok.name);
console.log(kodok.legs);
console.log(kodok.cold_blooded);
console.log(kodok.jump()); // "Hop Hop"

// No.2 Function to class
class NewClock{
    constructor({template}){
        this.timer;
        this.template = template;
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop(){
        clearInterval(this.timer);
    }

    start(){
        this.render();
        this.timer = setInterval(()=>this.render(), 1000);
    }
}

var newClock = new NewClock({ template: 'h:m:s' });
newClock.start();
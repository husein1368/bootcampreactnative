// No.1 if-else
console.log('No.1');
var nama = 'Husein';
var peran = 'Werewolf';

if (nama == '') {
    console.log('Nama Harus Diisi');
} else if (peran == '') {
    console.log('Peran Harus Diisi');
} else if (nama != '' && peran == '') {
    console.log('Halo' + nama + ', Pilih peranmu untuk memulai game!');
} else {
    console.log('Selamat datang di Dunia Werewolf, ' + nama);
    if (peran == 'Penyihir') {
        console.log('Halo Penyihir ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!')
    } else if (peran == 'Guard') {
        console.log('Halo Guard ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf.');
    } else if (peran == 'Werewolf') {
        console.log('Halo Werewolf ' + nama + ', Kamu akan memakan mangsa setiap malam!');
    } else {
        console.log('Halo ' + nama + ', Peran yang kamu masukkan tidak terdaftar, Silahkan pilih peran lain!');
    }
}

// No.2 SwitchCase
console.log('No.2');
var hari = 11;
var bulan = 1;
var tahun = 2021;

var day = 0;
var month = '';
var year = 0;

if (hari >= 1 && hari <= 31) {
    day = hari;
}

if (bulan >= 1 && bulan <= 12) {
    switch (bulan) {
        case 1: {
            month = 'Januari';
            break;
        }
        case 2: {
            month = 'Februari';
            break;
        }
        case 3: {
            month = 'Maret';
            break;
        }
        case 4: {
            month = 'April';
            break;
        }
        case 5: {
            month = 'Mei';
            break;
        }
        case 6: {
            month = 'Juni';
            break;
        }
        case 7: {
            month = 'Juli';
            break;
        }
        case 8: {
            month = 'Agustus';
            break;
        }
        case 9: {
            month = 'September';
            break;
        }
        case 10: {
            month = 'Oktober';
            break;
        }
        case 11: {
            month = 'November';
            break;
        }
        case 12: {
            month = 'Desember';
            break;
        }
    }
}

if (tahun >= 1900 && tahun <= 2200) {
    year = tahun;
}

var date = day + " " + month + " " + year;
console.log(date);